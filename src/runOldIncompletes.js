require("dotenv").config();

let Enrollment = require("../entities/enrollment").default;
const logger = require("../libs/logger").default;
let asyncPool = require("tiny-async-pool");
const db = require("../libs/db");
const helper = require("../libs/helper");

const segFaultHandler = require("segfault-handler");
segFaultHandler.registerHandler("../logs/crash.log", helper.segFaultHandler);

const metaData = {
	entity: "main",
	action: "system",
	process: "runOldIncompletes",
};

exports.main = function () {
	db.initDb()
		.catch(function (err) {
			logger.emerg("Unable to connect to the db", {
				entity: "main",
				action: "database",
				err,
			});
			process.exit(1);
		})
		.then(function () {
			return run();
		})
		.then(function () {
			process.exit(0);
		});
};

function run() {
	logger.info("Start of processing incomplete learners", metaData);
	return new Promise(function (resolve) {
		db.queryDb(
			`Select ${Enrollment.dbFields.join(",")} From LMS_ENROLL_OLD_INCOMPLETES`
		)
			.then(function (enrollments) {
				return asyncPool(
					Number(process.env.CALLS_LIMIT),
					enrollments,
					function (enrollment) {
						return Enrollment.processUpdateEntity(enrollment);
					}
				);
			})
			.then(function () {
				logger.info("End of processing incomplete learners", metaData);
			})
			.catch(function (err) {
				let error = err;
				if (err.message) {
					error = err.message;
				}
				logger.crit("Error processing incomplete learners", {
					...metaData,
					error,
				});
			})
			.finally(function () {
				resolve();
			});
	});
}

if (require.main === module) {
	exports.main();
}

process.once("SIGTERM", function () {
	return terminate("SIGTERM");
});
process.once("SIGINT", function () {
	return terminate("SIGINT");
});

function terminate(signal) {
	logger.crit(`Received ${signal} Termination Signal`, {
		entity: "main",
		action: "system",
		process: "runEntities",
		signal,
	});

	db.closeDb()
		.then(function () {
			logger.info("Terminate: Database closed", {
				entity: "main",
				action: "system",
				process: "runEntities",
			});
			process.exit(0);
		})
		.catch(function (err) {
			logger.error("Terminate: Unable to close the database ", {
				entity: "main",
				error: err,
				action: "system",
				process: "runEntities",
			});
			process.exit(1);
		});
}
