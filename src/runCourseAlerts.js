require("dotenv").config();

const logger = require("../libs/logger").default;
const db = require("../libs/db");
const helper = require("../libs/helper");
const Axios = require("axios");
let asyncPool = require("tiny-async-pool");
const os = require("os");

const metaData = {
	entity: "main",
	action: "system",
	process: "runCourseAlerts",
};

let axios = Axios.create({
	baseURL:
		"https://" + process.env.SPLUNK_HOST + ":8088/services/collector/event",
	timeout: Number(process.env.HTTP_REQUEST_TIMEOUT),
	headers: {
		Authorization: "Splunk " + process.env.SPLUNK_TOKEN,
	},
});

const splunkMetaData = {
	index: process.env.SPLUNK_INDEX,
	sourcetype: "_json",
	host: os.hostname(),
	source: "course_alerts",
};

const dbFields = [
	"CODE",
	"TITLE",
	"INSTITUTION",
	"TERM",
	"CLASS_NUMBER",
	"SESSION_CODE",
	"IS_COMBINED",
	"ISSUE",
];

const segFaultHandler = require("segfault-handler");
segFaultHandler.registerHandler("../logs/crash.log", helper.segFaultHandler);

exports.main = function () {
	db.initDb()
		.catch(function (err) {
			logger.emerg("Unable to connect to the db", {
				entity: "main",
				action: "database",
				process: "runCourseAlerts",
				err,
			});
			process.exit(1);
		})
		.then(function () {
			return run();
		});
};

if (require.main === module) {
	exports.main();
}

function run() {
	logger.info("Start of course alerts", metaData);

	const alertViews = process.env.COURSE_ALERTS_TO_RUN.split(",");

	asyncPool(1, alertViews, function (view) {
		return processViewCourses(view);
	})
		.then(function () {
			db.closeDb()
				.then(function () {
					logger.info("All database connections closed", metaData);
				})
				.catch(function (err) {
					logger.info("Problem closing all database connections", {
						...metaData,
						action: "database",
						err,
					});
				})
				.then(function () {
					logger.info("Done", metaData);
					process.exit(0);
				});
		})
		.catch(function (error) {
			logger.warning("Unknown error on course alerts ", {
				...metaData,
				error,
				result: "error",
			});
			process.exit(1);
		});
}

function processViewCourses(view) {
	logger.info(`Start of processing ${view} course issues`, {
		...metaData,
		entity: view,
	});
	return new Promise(function (resolve) {
		db.queryDb(`Select ${dbFields.join(",")} From LMS_COURSE_ISSUE_${view}`)
			.then(function (courses) {
				return asyncPool(
					Number(process.env.CALLS_LIMIT),
					courses,
					function (course) {
						return processCourse(course);
					}
				);
			})
			.then(function () {
				logger.info(`End of processing ${view} course issues`, {
					...metaData,
					entity: view,
				});
			})
			.catch(function (err) {
				let error = err;
				if (err.message) {
					error = err.message;
				}
				logger.crit(`Error processing ${view} course issues`, {
					...metaData,
					entity: view,
					error,
				});
			})
			.finally(function () {
				resolve();
			});
	});
}

function processCourse(course) {
	return new Promise(function (resolve) {
		logger.info("Potential Course Issue", {
			...metaData,
			course,
		});
		sendSplunkMessage(course)
			.then(function (course) {
				return markCourseAlertAsCompleted(course);
			})
			.catch(function (err) {
				logger.notice("Unable to process course alert", {
					...metaData,
					err,
				});
			})
			.finally(function () {
				resolve();
			});
	});
}

function markCourseAlertAsCompleted(course) {
	let fieldString = dbFields.join(", ");
	let valueString = ":" + dbFields.join(", :");
	let whereString = dbFields.map(function (f) {
		return `${f} = :${f}`;
	});
	whereString = whereString.join(" And ");

	let sql = `Insert into COURSE_ISSUES (${fieldString}) Select ${valueString} From dual Where Not Exists (Select * From course_issues where ${whereString})`;

	return db.executeSQL(sql, course, true);
}

function sendSplunkMessage(course) {
	return new Promise(function (resolve, reject) {
		axios
			.post("", { ...splunkMetaData, event: course })
			.then(function (res) {
				if (res.data && res.data.text && res.data.text === "Success") {
					return resolve(course);
				}
				reject({ message: "200, but not success" });
			})
			.catch(function (err) {
				reject(err);
			});
	});
}

process.once("SIGTERM", function () {
	return terminate("SIGTERM");
});
process.once("SIGINT", function () {
	return terminate("SIGINT");
});

function terminate(signal) {
	logger.crit(`Received ${signal} Termination Signal`, {
		entity: "main",
		action: "system",
		process: "runCourseAlerts",
		signal,
	});

	db.closeDb()
		.then(function () {
			logger.info("Terminate: Database closed", {
				entity: "main",
				action: "system",
				process: "runCourseAlerts",
			});
			process.exit(0);
		})
		.catch(function (err) {
			logger.error("Terminate: Unable to close the database ", {
				entity: "main",
				error: err,
				action: "system",
				process: "runCourseAlerts",
			});
			process.exit(1);
		});
}
