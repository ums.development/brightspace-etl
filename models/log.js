const db = require("../libs/db");
const dayjs = require("dayjs");
const logger = require("../libs/logger").default;

exports.addLog = function (
	entity,
	umsId,
	lmsId,
	action,
	statusCode,
	message,
	data
) {
	return new Promise(function (resolve, reject) {
		let dbConn;

		db.getConnection()
			.catch(function (err) {
				logger.error("Error getting connection to add log", {
					entity: "main",
					action: "database",
					error: err,
				});
				throw new Error("Unable to get a db connection");
			})
			.then(function makeConnectionGlobal(databaseConnection) {
				dbConn = databaseConnection;
			})
			.then(function logEntry() {
				let fields = [
					"date_time",
					"entity",
					"ums_id",
					"action",
					"status_code",
					"message",
				];
				let dt = dayjs().format("YYYY-MM-DD HH:mm:ss");
				let sc = "";
				if (statusCode) {
					sc = statusCode.toString();
				}
				let insertData = [dt, entity, umsId, action, sc, message];
				if (lmsId) {
					fields.push("lms_id");
					insertData.push(lmsId);
				}
				if (data) {
					fields.push("data");
					insertData.push(JSON.stringify(data));
				}

				let sql = "";
				fields.forEach(function (field) {
					sql += ", " + field;
				});
				let fieldString = sql.substr(2);
				let valueString = sql.replace(new RegExp(", ", "g"), ", :").substr(2);

				sql =
					"Insert into log (" + fieldString + ") values(" + valueString + ")";
				return dbConn.execute(sql, insertData, { autoCommit: true });
			})
			.then(function processResults(results) {
				resolve(results.rowsAffected);
			})
			.catch(function (error) {
				reject(error);
			})
			.finally(function closeDatabase() {
				dbConn.close();
			})
			.catch(function (err) {
				logger.notice("Unable to close database connection: ", {
					entity: "main",
					action: "database",
					error: err.message,
				});
			});
	});
};
