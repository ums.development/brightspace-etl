const db = require("../libs/db");
const logger = require("../libs/logger").default;

exports.lock = function (Entity) {
	let sql =
		"Update entity_lock Set is_locked = 1 Where entity = :entity And is_locked = 0";
	return new Promise(function (resolve, reject) {
		db.executeSQL(sql, { entity: Entity.slug }, true).then(
			function processResults(rowsAffected) {
				if (rowsAffected === 1) {
					resolve(1);
				} else {
					updateLockStatus(Entity.slug).finally(function () {
						reject(
							"Unable to obtain a lock, updated " + rowsAffected + " rows"
						);
					});
				}
			}
		);
	});
};

function updateLockStatus(entitySlug) {
	return new Promise(function (resolve, reject) {
		let sql = "Select is_locked from entity_lock Where entity = :entity";
		db.queryDb(sql, { entity: entitySlug })
			.then(function logLockCount(rows) {
				const locks = rows[0].IS_LOCKED;

				let logLevel = "crit";
				if (locks >= Number(process.env.ENTITY_LOCK_THRESHOLD_ALERT)) {
					logLevel = "alert";
				}
				if (locks >= Number(process.env.ENTITY_LOCK_THRESHOLD_EMERG)) {
					logLevel = "emerg";
				}

				logger.log(
					logLevel,
					entitySlug + ": entity has " + locks + " locks, incrementing",
					{
						entity: entitySlug,
						action: "update",
					}
				);
			})
			.then(function incrementLockStatus() {
				let updateSql =
					"Update entity_lock Set is_locked = is_locked + 1 Where entity = :entity And is_locked > 0";
				return db.executeSQL(updateSql, [entitySlug]);
			})
			.then(function processResults(rowsAffected) {
				if (rowsAffected !== 1) {
					logger.alert(
						entitySlug +
							": Unable able to increment entity lock, updated " +
							rowsAffected +
							" rows",
						{
							entity: entitySlug,
							action: "update",
						}
					);
				}
			})
			.catch(function (error) {
				logger.alert("Error updating entity lock status", {
					entity: entitySlug,
					action: "update",
					error: error,
				});
			});
	});
}

exports.release = function (Entity) {
	return new Promise(function (resolve, reject) {
		let sql = "Update entity_lock Set is_locked = 0 Where entity = :entity";
		db.executeSQL(sql, { entity: Entity.slug }).then(function processResults(
			rowsAffected
		) {
			if (rowsAffected === 1) {
				resolve(1);
			} else {
				reject("Unable to release a lock, updated " + rowsAffected + " rows");
			}
		});
	});
};

exports.releaseAll = function () {
	let sql = "Update entity_lock Set is_locked = 0";
	return db.executeSQL(sql);
};

exports.updateTermBeingProcesses = function (Entity, term) {
	return new Promise(function (resolve, reject) {
		let sql = "Update entity_lock Set TERM = :term Where entity = :entity";
		db.executeSQL(sql, { term, entity: Entity.slug }).then(
			function processResults(rowsAffected) {
				if (rowsAffected === 1) {
					resolve(1);
				} else {
					reject(
						"Unable to update the " +
							Entity.slug +
							" term being processed " +
							rowsAffected +
							" rows"
					);
				}
			}
		);
	});
};
