const db = require("../libs/db");

exports.getTermsForProcessing = function () {
	let sql = "Select TERM from TERMS_TO_PROCESS Group By TERM Order By TERM";

	return new Promise(function (resolve, reject) {
		db.queryDb(sql)
			.then(function (results) {
				resolve(results);
			})
			.catch(function (error) {
				reject(error);
			});
	});
};
