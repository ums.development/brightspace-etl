let Base = require("./base").default;

let Sandbox = Object.create(Base);

Sandbox.name = "Sandbox";
Sandbox.slug = "sandbox";
Sandbox.endPoint = "sandbox";
Sandbox.dbFields = ["EMPLID"];
Sandbox.umsIdField = "EMPLID";
Sandbox.httpRequestTimeoutFactor = 6;

Sandbox.makeLmsEntity = function (dbUser) {
	return {
		userId: dbUser.EMPLID,
	};
};

Sandbox.getEntityFromDbUpdate = function () {
	return new Promise(function (resolve) {
		resolve([]);
	});
};

Sandbox.lmsViewName = function (queryType) {
	return "lms_" + this.slug + "es_" + queryType;
};

exports.default = Sandbox;
