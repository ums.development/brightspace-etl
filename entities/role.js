const db = require("../libs/db");
const { default: logger } = require("../libs/logger");
const dbLog = require("../models/log");
let Base = require("./base").default;

let Role = Object.create(Base);

Role.name = "System Role";
Role.slug = "role";
Role.endPoint = "role";
Role.dbFields = ["USER_LMS_ID", "ROLE_IND"];
Role.addGeneratesNewId = false;
Role.umsIdField = "USER_LMS_ID";
Role.searchByUmsId = true;

Role.makeLmsEntity = function (dbItem) {
	return {
		userLmsId: dbItem.USER_LMS_ID,
		role: dbItem.ROLE_IND,
	};
};

/*
	Since there isn't an update enrollment route, we can just use the processNewEntity to Post, instead of Put.
	Because of the way that cascading roles work, we had to delete a user's enrollment form UMS, effectively removing
		Them from all of their enrollments under the UMS tree. This will delete all of their known enrollments
		(enrollment_completed table) so that when the integration runs again for enrollments, it will re-enroll that
		user into their courses. The one caveat here, is that this will only work for those courses in campus/terms
		that are being processed.
 */
Role.processUpdateEntity = function (dbEntity) {
	return new Promise(function (resolve) {
		Role.processNewEntity(dbEntity)
			.then(function () {
				let sql =
					"Delete From ENROLLMENT_COMPLETED Where USER_LMS_ID = '" +
					dbEntity[Role.umsIdField] +
					"'";

				return db.executeSQL(sql);
			})
			.then(function (rowsAffected) {
				return dbLog.addLog(
					Role.slug,
					dbEntity[Role.umsIdField],
					dbEntity[Role.umsIdField],
					"put",
					null,
					"Reset user's enrollments for reprocessing",
					{
						role_ind: dbEntity.ROLE_IND,
						rows_affected: rowsAffected,
					}
				);
			})
			.catch(function (error) {
				logger.warning("Error on update", {
					entity: Role.slug,
					action: "put",
					dbEntity: dbEntity,
					error: error,
				});
				return dbLog.addLog(
					Role.slug,
					dbEntity[Role.umsIdField],
					dbEntity[Role.umsIdField],
					"put",
					null,
					"Errored on update",
					null
				);
			})
			.finally(function () {
				resolve();
			});
	});
};

exports.default = Role;
