let Base = require("./base").default;

let Semester = Object.create(Base);

Semester.name = "Semester";
Semester.slug = "semester";
Semester.endPoint = "semester";
Semester.dbFields = ["CODE", "NAME"];

Semester.makeLmsEntity = function (dbUser) {
	return {
		code: dbUser.CODE,
		name: dbUser.NAME,
	};
};

Semester.getEntityFromDbUpdate = function () {
	return new Promise(function (resolve) {
		resolve([]);
	});
};

exports.default = Semester;
