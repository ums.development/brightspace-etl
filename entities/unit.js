let Base = require("./base").default;

let College = Object.create(Base);

College.name = "Academic Unit";
College.slug = "unit";
College.endPoint = "organization";
College.dbFields = ["CODE", "NAME", "PARENT"];

College.makeLmsEntity = function (dbItem) {
	return {
		code: dbItem.CODE,
		name: dbItem.NAME,
		type: "unit",
		parent: dbItem.PARENT,
	};
};

College.getEntityFromDbUpdate = function () {
	return new Promise(function (resolve) {
		resolve([]);
	});
};

exports.default = College;
