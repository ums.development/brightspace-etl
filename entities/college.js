let Base = require("./base").default;

let College = Object.create(Base);

College.name = "College";
College.slug = "college";
College.endPoint = "organization";
College.dbFields = ["CODE", "NAME", "PARENT"];

College.makeLmsEntity = function (dbItem) {
	return {
		code: dbItem.CODE,
		name: dbItem.NAME,
		type: "college",
		parent: dbItem.PARENT,
	};
};

College.getEntityFromDbUpdate = function () {
	return new Promise(function (resolve) {
		resolve([]);
	});
};

exports.default = College;
