const helper = require("./helper");
const oracle = require("oracledb");
const logger = require("./logger").default;

exports.initDb = function () {
	return new Promise(function (resolve, reject) {
		const creds = helper.databaseCredentials();
		oracle
			.createPool({
				user: creds.user,
				password: creds.password,
				connectString: creds.connectString,
				poolMax: 4,
				poolMin: 0,
				poolIncrement: 1,
				poolPingInterval: 50,
				poolTimeout: 50,
			})
			.then(function (pool) {
				resolve(pool.status);
			})
			.catch(function (err) {
				reject(err.message);
			});
	});
};

exports.getConnection = function () {
	return new Promise(function (resolve, reject) {
		let dbConn = null;
		oracle
			.getConnection()
			.catch(function () {
				reject("Error getting connection from pool");
			})
			.then(function initiateDbConn(conn) {
				dbConn = conn;
				return conn.execute(
					"alter session set nls_timestamp_format = 'YYYY-MM-DD HH24:MI:SS'"
				);
			})
			.then(function () {
				return dbConn.execute(
					"ALTER SESSION SET NLS_DATE_FORMAT = 'YYYY-MM-DD'"
				);
			})
			.then(function () {
				resolve(dbConn);
			})
			.catch(function () {
				reject("Error initiating connection");
			});
	});
};

exports.closeDb = function () {
	return new Promise(function (resolve, reject) {
		oracle
			.getPool()
			.close(3)
			.then(function () {
				resolve();
			})
			.catch(function (err) {
				reject(err.message);
			});
	});
};

exports.queryDb = function (sql, data = [], returnAsObject = true) {
	let dbConn;

	return new Promise(function (resolve, reject) {
		exports
			.getConnection()
			.catch(function (err) {
				return reject("Error getting connection: " + err);
			})
			.then(function executeSQL(databaseConnection) {
				dbConn = databaseConnection;

				let options = {};
				if (returnAsObject) {
					options.outFormat = oracle.OUT_FORMAT_OBJECT;
				}

				return dbConn.execute(sql, data, options);
			})
			.then(function processResults(results) {
				resolve(results.rows);
			})
			.catch(function (error) {
				reject(error);
			})
			.finally(function closeDatabase() {
				dbConn.close();
			})
			.catch(function (err) {
				logger.notice("Unable to close database connection: ", {
					entity: "main",
					action: "database",
					error: err,
				});
			});
	});
};

exports.executeSQL = function (sql, data = [], autoCommit = true) {
	let dbConn;

	return new Promise(function (resolve, reject) {
		exports
			.getConnection()
			.catch(function (err) {
				return reject("Error getting connection: " + err);
			})
			.then(function executeSQL(databaseConnection) {
				dbConn = databaseConnection;

				let options = {};
				if (autoCommit) {
					options.autoCommit = true;
				}

				return dbConn.execute(sql, data, options);
			})
			.then(function processResults(results) {
				resolve(results.rowsAffected);
			})
			.catch(function (error) {
				reject(error);
			})
			.finally(function closeDatabase() {
				dbConn.close();
			})
			.catch(function (err) {
				logger.notice("Unable to close database connection: ", {
					entity: "main",
					action: "database",
					error: err.message,
				});
			});
	});
};
