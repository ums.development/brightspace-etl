const Axios = require("axios");
const rateLimiter = require("axios-rate-limit");
const logger = require("../libs/logger").default;

let rlAxios = null;

function initAxios() {
	let axios = Axios.create({
		baseURL: process.env.LMS_ETL_HOST,
		timeout: Number(process.env.HTTP_REQUEST_TIMEOUT),
		headers: {
			Authorization: "Bearer " + process.env.JWT_TOKEN,
		},
	});

	axios.interceptors.response.use(
		interceptResponseSuccess,
		interceptResponseError
	);

	rlAxios = rateLimiter(axios, {
		maxRPS: process.env.HTTP_REQUESTS_PER_SECOND,
	});
}

function interceptResponseSuccess(res) {
	updateRateLimit(res);

	return res;
}

function interceptResponseError(err) {
	if (err.response) {
		updateRateLimit(err.response);

		return Promise.reject(err);
	} else {
		updateRateLimit(null);
		return Promise.reject(err);
	}
}

function updateRateLimit(response) {
	const defaultRequests = process.env.HTTP_REQUESTS_PER_SECOND;
	const defaultMilliseconds = 1000;

	if (!response) {
		setNewRateLimit(defaultRequests, defaultMilliseconds);
		return;
	}

	const apiRequestState =
		response.headers[process.env.HTTP_RATE_LIMIT_STATE_HEADER];
	const resetMilliseconds =
		response.headers[process.env.HTTP_RATE_LIMIT_RESET_HEADER] * 1000 + 3000;

	switch (apiRequestState) {
		case "throttle":
			setNewRateLimit(1, defaultMilliseconds);
			break;

		case "critical":
		case "halt":
			setNewRateLimit(1, resetMilliseconds);
			break;

		default:
			setNewRateLimit(defaultRequests, defaultMilliseconds);
	}
}

function setNewRateLimit(maxRequests, perMilliseconds) {
	rlAxios.setRateLimitOptions({
		maxRequests,
		perMilliseconds,
	});
}

exports.default = function () {
	if (!rlAxios) {
		initAxios();
	}

	return rlAxios;
};
